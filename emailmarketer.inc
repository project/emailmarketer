<?php


/**
 *
 */
function emailmarketer_list($list = NULL, $rights = FALSE) {
  global $user;
  $where = $lists = array();
  $order = ' ORDER BY email ASC, name ASC';
  if (is_numeric($list)) {
    $where[] = 'eid = %d';
  }
  if (is_string($list)) {
    $where[] = "email_subscribe = '%s'";
  }
  if ($rights && !user_access('administer email marketer')) {
    $like = array();
    foreach ($user->roles as $rid => $role) {
      $like[] = "roles LIKE '%|" . $rid . "|%'";
    }
    if (!empty($like)) {
      $where[] = '(' . implode(' OR ', $like) . ')';
    }
  }

  $where = empty($where) ? '' : ' WHERE ' . implode(' AND ', $where);
  $sql = "SELECT * FROM {" . EMAILMARKETER_TABLE . "}" . $where . $order;
  
  $result = db_query($sql, $list);
  while ($row = db_fetch_array($result)) {
    $lists[$row['eid']] = $row;
  }
  return $lists;
}

function emailmarketer_fields() {
  $countries = array("" => "[...]", "AF" => t("Afghanistan"), "AL" => t("Albania"), "DZ" => t("Algeria"), "AS" => t("American Samoa"), "AD" => t("Andorra"), "AO" => t("Angola"), "AI" => t("Anguilla"), "AQ" => t("Antarctica"), "AG" => t("Antigua and Barbuda"), "AR" => t("Argentina"), "AM" => t("Armenia"), "AW" => t("Aruba"), "AU" => t("Australia"), "AT" => t("Austria"), "AZ" => t("Azerbaijan"), "BS" => t("Bahamas"), "BH" => t("Bahrain"), "BD" => t("Bangladesh"), "BB" => t("Barbados"), "BY" => t("Belarus"), "BE" => t("Belgium"), "BZ" => t("Belize"), "BJ" => t("Benin"), "BM" => t("Bermuda"), "BT" => t("Bhutan"), "BO" => t("Bolivia"), "BA" => t("Bosnia and Herzegovina"), "BW" => t("Botswana"), "BV" => t("Bouvet Island"), "BR" => t("Brazil"), "IO" => t("British Indian Ocean"), "BN" => t("Brunei"), "BG" => t("Bulgaria"), "BF" => t("Burkina Faso"), "BI" => t("Burundi"), "KH" => t("Cambodia"), "CM" => t("Cameroon"), "CA" => t("Canada"), "CV" => t("Cape Verde"), "KY" => t("Cayman Islands"), "CF" => t("Central African Republic"), "TD" => t("Chad"), "CL" => t("Chile"), "CN" => t("China"), "CX" => t("Christmas Island"), "CC" => t("Cocos (Keeling) Islands"), "CO" => t("Colombia"), "KM" => t("Comoros"), "CG" => t("Congo"), "CK" => t("Cook Islands"), "CR" => t("Costa Rica"), "CI" => t("C&ocirc;te d'Ivoire"), "HR" => t("Croatia (Hrvatska)"), "CU" => t("Cuba"), "CY" => t("Cyprus"), "CZ" => t("Czech Republic"), "CD" => t("Congo (DRC)"), "DK" => t("Denmark"), "DJ" => t("Djibouti"), "DM" => t("Dominica"), "DO" => t("Dominican Republic"), "TP" => t("East Timor"), "EC" => t("Ecuador"), "EG" => t("Egypt"), "SV" => t("El Salvador"), "GQ" => t("Equatorial Guinea"), "ER" => t("Eritrea"), "EE" => t("Estonia"), "ET" => t("Ethiopia"), "FK" => t("Falkland Islands (Islas Malvinas)"), "FO" => t("Faroe Islands"), "FJ" => t("Fiji Islands"), "FI" => t("Finland"), "FR" => t("France"), "GF" => t("French Guiana"), "PF" => t("French Polynesia"), "TF" => t("French Southern and Antarctic Lands"), "GA" => t("Gabon"), "GM" => t("Gambia"), "GE" => t("Georgia"), "DE" => t("Germany"), "GH" => t("Ghana"), "GI" => t("Gibraltar"), "GR" => t("Greece"), "GL" => t("Greenland"), "GD" => t("Grenada"), "GP" => t("Guadeloupe"), "GU" => t("Guam"), "GT" => t("Guatemala"), "GN" => t("Guinea"), "GW" => t("Guinea-Bissau"), "GY" => t("Guyana"), "HT" => t("Haiti"), "HM" => t("Heard Island and McDonald Islands"), "HN" => t("Honduras"), "HK" => t("Hong Kong SAR"), "HU" => t("Hungary"), "IS" => t("Iceland"), "IN" => t("India"), "ID" => t("Indonesia"), "IR" => t("Iran"), "IQ" => t("Iraq"), "IE" => t("Ireland"), "IL" => t("Israel"), "IT" => t("Italy"), "JM" => t("Jamaica"), "JP" => t("Japan"), "JO" => t("Jordan"), "KZ" => t("Kazakhstan"), "KE" => t("Kenya"), "KI" => t("Kiribati"), "KR" => t("Korea"), "KW" => t("Kuwait"), "KG" => t("Kyrgyzstan"), "LA" => t("Laos"), "LV" => t("Latvia"), "LB" => t("Lebanon"), "LS" => t("Lesotho"), "LR" => t("Liberia"), "LY" => t("Libya"), "LI" => t("Liechtenstein"), "LT" => t("Lithuania"), "LU" => t("Luxembourg"), "MO" => t("Macao SAR"), "MK" => t("Macedonia, Former Yugoslav Republic of"), "MG" => t("Madagascar"), "MW" => t("Malawi"), "MY" => t("Malaysia"), "MV" => t("Maldives"), "ML" => t("Mali"), "MT" => t("Malta"), "MH" => t("Marshall Islands"), "MQ" => t("Martinique"), "MR" => t("Mauritania"), "MU" => t("Mauritius"), "YT" => t("Mayotte"), "MX" => t("Mexico"), "FM" => t("Micronesia"), "MD" => t("Moldova"), "MC" => t("Monaco"), "MN" => t("Mongolia"), "MS" => t("Montserrat"), "MA" => t("Morocco"), "MZ" => t("Mozambique"), "MM" => t("Myanmar"), "NA" => t("Namibia"), "NR" => t("Nauru"), "NP" => t("Nepal"), "NL" => t("Netherlands"), "AN" => t("Netherlands Antilles"), "NC" => t("New Caledonia"), "NZ" => t("New Zealand"), "NI" => t("Nicaragua"), "NE" => t("Niger"), "NG" => t("Nigeria"), "NU" => t("Niue"), "NF" => t("Norfolk Island"), "KP" => t("North Korea"), "MP" => t("Northern Mariana Islands"), "NO" => t("Norway"), "OM" => t("Oman"), "PK" => t("Pakistan"), "PW" => t("Palau"), "PA" => t("Panama"), "PG" => t("Papua New Guinea"), "PY" => t("Paraguay"), "PE" => t("Peru"), "PH" => t("Philippines"), "PN" => t("Pitcairn Islands"), "PL" => t("Poland"), "PT" => t("Portugal"), "PR" => t("Puerto Rico"), "QA" => t("Qatar"), "RE" => t("Reunion"), "RO" => t("Romania"), "RU" => t("Russia"), "RW" => t("Rwanda"), "WS" => t("Samoa"), "SM" => t("San Marino"), "ST" => t("S&atilde;o Tom¨¦ and Pr¨Şncipe"), "SA" => t("Saudi Arabia"), "SN" => t("Senegal"), "YU" => t("Serbia and Montenegro"), "SC" => t("Seychelles"), "SL" => t("Sierra Leone"), "SG" => t("Singapore"), "SK" => t("Slovakia"), "SI" => t("Slovenia"), "SB" => t("Solomon Islands"), "SO" => t("Somalia"), "ZA" => t("South Africa"), "GS" => t("South Georgia and the South Sandwich Islands"), "ES" => t("Spain"), "LK" => t("Sri Lanka"), "SH" => t("St. Helena"), "KN" => t("St. Kitts and Nevis"), "LC" => t("St. Lucia"), "PM" => t("St. Pierre and Miquelon"), "VC" => t("St. Vincent and the Grenadines"), "SD" => t("Sudan"), "SR" => t("Suriname"), "SJ" => t("Svalbard and Jan Mayen"), "SZ" => t("Swaziland"), "SE" => t("Sweden"), "CH" => t("Switzerland"), "SY" => t("Syria"), "TW" => t("Taiwan"), "TJ" => t("Tajikistan"), "TZ" => t("Tanzania"), "TH" => t("Thailand"), "TG" => t("Togo"), "TK" => t("Tokelau"), "TO" => t("Tonga"), "TT" => t("Trinidad and Tobago"), "TN" => t("Tunisia"), "TR" => t("Turkey"), "TM" => t("Turkmenistan"), "TC" => t("Turks and Caicos Islands"), "TV" => t("Tuvalu"), "UG" => t("Uganda"), "UA" => t("Ukraine"), "AE" => t("United Arab Emirates"), "UK" => t("United Kingdom"), "US" => t("United States"), "UM" => t("United States Minor Outlying Islands"), "UY" => t("Uruguay"), "UZ" => t("Uzbekistan"), "VU" => t("Vanuatu"), "VA" => t("Vatican City"), "VE" => t("Venezuela"), "VN" => t("Viet Nam"), "VG" => t("Virgin Islands (British)"), "VI" => t("Virgin Islands"), "WF" => t("Wallis and Futuna"), "YE" => t("Yemen"), "ZM" => t("Zambia"), "ZW" => t("Zimbabwe"));
  return array(
    'FullName' => array(
      '#type' => 'textfield',
      '#title' => t('Full name'),
      '#maxlength' => 255
    ),
    'FirstName' => array(
      '#type' => 'textfield',
      '#title' => t('First name'),
      '#maxlength' => 255
    ),
    'LastName' => array(
      '#type' => 'textfield',
      '#title' => t('Last name'),
      '#maxlength' => 255
    ),
    'NickName' => array(
      '#type' => 'textfield',
      '#title' => t('Nickname'),
      '#maxlength' => 255
    ),
    'Email2' => array(
      '#type' => 'textfield',
      '#title' => t('Additional e-mail'),
      '#maxlength' => 255
    ),
    'Identity' => array(
      '#type' => 'textfield',
      '#title' => t('Identity'),
      '#maxlength' => 255
    ),
    'Gender' => array(
      '#type' => 'select',
      '#title' => t('Gender'),
      '#options' => array('M' => t('Male'), 'F' => t('Female'))
    ),
    'Birthday' => array(
      '#type' => 'date',
      '#title' => t('Birthday'),
    ),
    'Marriage' => array(
      '#type' => 'select',
      '#title' => t('Marriage status'),
      '#options' => array('0' => t('No'), '1' => t('Yes'), '2' => t('Unknown'))
    ),
    'Mobile' => array(
      '#type' => 'textfield',
      '#title' => t('Mobile'),
      '#maxlength' => 255
    ),
    'Company' => array(
      '#type' => 'textfield',
      '#title' => t('Company'),
      '#maxlength' => 255
    ),
    'Country' => array(
      '#type' => 'select',
      '#title' => t('Business country'),
      '#options' => $countries
    ),
    'State' => array(
      '#type' => 'textfield',
      '#title' => t('Business state'),
      '#maxlength' => 255
    ),
    'City' => array(
      '#type' => 'textfield',
      '#title' => t('Business city'),
      '#maxlength' => 255
    ),
    'Address' => array(
      '#type' => 'textfield',
      '#title' => t('Business address'),
      '#maxlength' => 255
    ),
    'Zipcode' => array(
      '#type' => 'textfield',
      '#title' => t('Business zip code'),
      '#maxlength' => 255
    ),
    'Tel' => array(
      '#type' => 'textfield',
      '#title' => t('Business phone'),
      '#maxlength' => 255
    ),
    'Fax' => array(
      '#type' => 'textfield',
      '#title' => t('Business fax'),
      '#maxlength' => 255
    ),
    'Homepage' => array(
      '#type' => 'textfield',
      '#title' => t('Business homepage'),
      '#maxlength' => 255
    ),
    'Title' => array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#maxlength' => 255
    ),
    'Occupation' => array(
      '#type' => 'select',
      '#title' => t('Occupation'),
      '#options' => array('A' => t('Accounting/Finance'), 'I' => t('Computer related (other)'), 'W' => t('Computer related (Internet)'), 'C' => t('Consulting'), 'V' => t('Customer service/support'), 'T' => t('Education/training'), 'E' => t('Engineering'), 'O' => t('Executive/senior management'), 'G' => t('General administrative/supervisory'), 'X' => t('Government/military'), 'H' => t('Homemaker'), 'M' => t('Manufacturing/production/operations'), 'P' => t('Professional (medical, legal, etc.)'), 'R' => t('Research and development'), 'F' => t('Retired'), 'S' => t('Sales/marketing/advertising'), 'L' => t('Self-employed/owner'), 'D' => t('Student'), 'B' => t('Tradesman/craftsman'), 'U' => t('Unemployed/between jobs'), '9' => t('Other'))
    ),
    'Department' => array(
      '#type' => 'textfield',
      '#title' => t('Department'),
      '#maxlength' => 255
    ),
    'Country2' => array(
      '#type' => 'select',
      '#title' => t('Home country'),
      '#options' => $countries
    ),
    'State2' => array(
      '#type' => 'textfield',
      '#title' => t('Home state'),
      '#maxlength' => 255
    ),
    'City2' => array(
      '#type' => 'textfield',
      '#title' => t('Home city'),
      '#maxlength' => 255
    ),
    'Address2' => array(
      '#type' => 'textfield',
      '#title' => t('Home address'),
      '#maxlength' => 255
    ),
    'Zipcode2' => array(
      '#type' => 'textfield',
      '#title' => t('Home zip code'),
      '#maxlength' => 255
    ),
    'Tel2' => array(
      '#type' => 'textfield',
      '#title' => t('Home phone'),
      '#maxlength' => 255
    ),
    'Fax2' => array(
      '#type' => 'textfield',
      '#title' => t('Home fax'),
      '#maxlength' => 255
    ),
    'Homepage2' => array(
      '#type' => 'textfield',
      '#title' => t('Personal homepage'),
      '#maxlength' => 255
    )
  );

}
