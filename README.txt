
Short Description
=================
This module allows visitors to subscribe to Nesox Email Marketer newsletters.
Nesox Email Marketer is a professional email newsletter software and opt-in email
marketing software for targeted mailing list building, email campaigns creating,
bulk mailing, and open and click tracking at your own computer. For more
informations about Nesox Email Marketer visit Nesox Website. This module
is not a newsletter generator! This module is useless if don't
use Nesox Email Marketer.

Administrators are allowed to add Email Marketer newsletters. They can
choose what fields will be available to subscribers, which of them will
be optional and which will be mandatory. The module provide a block and a page
where visitors can choose the newsletter they want to subscribe to. The block
and the page can be themed. All subscriptions are placed in a queue for
e-mail validation. The subscriber will receive an email with all informations
needed for completing the validation process. Finally the subscription is
sent by mail to the Email Marketer application.

Installation
============
Place the module in the modules directory and enable the modul from the
administrator menu. You can grant admin rights to other roles by using the
"Access Control" menu.
